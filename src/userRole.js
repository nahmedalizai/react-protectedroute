import React from 'react';
import {AuthConsumer} from './auth'

function UserRole() {
    const [isCRUD,setIsCRUD] = React.useState(false);
    const [isQCRU,setIsQCRU] = React.useState(false);
    const [isACRU,setIsACRU] = React.useState(false);
    const [isQCR,setIsQCR] = React.useState(false);
    const [isACR,setIsACR] = React.useState(false);

    function getRole(role,team) {
        if(role === 'CRUD')
            setIsCRUD(true) //admin user role
        else if(role === 'CRU' && team === 'QTN')
            setIsQCRU(true) //question team lead user role
        else if(role === 'CRU' && team === 'ANS')
            setIsACRU(true) //answer team lead user role
        else if(role === 'CR' && team === 'QTN')
            setIsQCR(true) //question team user role
        else if(role === 'CR' && team === 'ANS')
            setIsACR(true) //answer team user role
    }
  return <div>
            <AuthConsumer>
                {({user}) =>
                    getRole(user.role,user.team)
                }
            </AuthConsumer>
            {{isCRUD,isQCRU,isACRU,isQCR,isACR}}
        </div>;
}

export default UserRole;