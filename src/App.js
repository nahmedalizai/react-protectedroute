import React from 'react';
import './App.css';
import './config';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import LandingPage from './landingpage';
import Dashboard from './components/dashboard';
import ItemDetails from  './components/questiondetails';
import ProtectedRoute from './components/protectedroute';
import {Route, Switch, Router} from  'react-router-dom';
import {Navbar} from 'react-bootstrap';
import {AuthProvider,AuthConsumer} from './auth';
import history from './history';
import {FaUserCircle, FaSignOutAlt} from 'react-icons/fa';
import Button from '@material-ui/core/Button'
import Tooltip from '@material-ui/core/Tooltip'

function App() {

  return (
    <div>
          <AuthProvider>
            <Navbar fixed='top' color="light" expand="md">
              {/* <img src={logo}/> */}
              <span className='navbarBrand'>QUERY PLATFORM</span>
                <Navbar.Collapse></Navbar.Collapse>
                <AuthConsumer>
                  {({user,isAuthenticated,logout}) => (
                    isAuthenticated ? 
                      <div>
                        <label className='userLabel'><FaUserCircle/>{user.displayname}</label>
                        <Tooltip title="Log out">
                            <Button variant='contained' color='default' size='medium' onClick={logout}>
                                <FaSignOutAlt/></Button>
                        </Tooltip>
                      </div> : <div></div>      
                  )}
                </AuthConsumer>
            </Navbar>
            <div className='content'>
              <Router history={history}>
                <Switch>
                  <Route exact path="/" component={LandingPage} />
                  <ProtectedRoute exact path="/app" component={Dashboard} />
                  <ProtectedRoute exact path="/question" component={ItemDetails} />
                  <Route path="*" component={LandingPage} />
                </Switch>
              </Router>
            </div>
          </AuthProvider>
    </div>
  );
}

export default App;