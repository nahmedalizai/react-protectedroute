import React, {useEffect} from 'react'
import LightBox from 'react-image-lightbox'
import 'react-image-lightbox/style.css'
import {FaPaperclip} from 'react-icons/fa'
import config from 'react-global-configuration'
import axios from 'axios'
import { useAlert } from 'react-alert'

const LightBoxComp = (props) => {
    const reactAlert = useAlert()
    const [isOpen, setIsOpen] = React.useState(false);
    const [path, setPath] = React.useState('');
    
    useEffect(() => {
        if(props.attachmentid === null) { // eslint-disable-next-line
            //document.getElementById("attachmentButton").disabled = true
            document.getElementById("attachmentButton").style.display = "none"  // eslint-disable-next-line
        } // eslint-disable-next-line
    }, [])

    function getAttachment() {
        if(props.attachmentid !== null){
            var bodyFormData = new FormData();  
                bodyFormData.set('method', config.get('apiMethods').getAttachment);
                axios({
                    method: 'get',
                    url: config.get('apiurl') + config.get('apiMethods').getAttachment,
                    data: bodyFormData,
                    params: {
                       id: props.attachmentid
                    },
                    config: { headers: {'Content-Type': 'application/json'}}
                })
                .then((response) => {
                    if(response != null && response.status === 200)
                    {
                        if(response.data !== null && response.data.length > 0){
                            var file = response.data[0].vrp_attachment
                            var arrayBufferView = new Uint8Array( file.data );
                            var blob = new Blob( [ arrayBufferView ], { type: "image/jpeg" } );
                            var urlCreator = window.URL || window.webkitURL;
                            var imageUrl = urlCreator.createObjectURL( blob );
                            setPath(imageUrl)
                            setIsOpen(true)
                        }
                    }
                    else {
                        alert('Error in getting attachment')
                    }
                })
            .catch((error) => { alert(error) })
        }
        else
            reactAlert.error('No attachment found')
    }

    return (
        <div>
            <button id="attachmentButton" className='btn btn-sm btn-info' type="button" onClick={getAttachment}>
                view <FaPaperclip/>
            </button>
            { isOpen && (<div className='content'><LightBox 
                                mainSrc={path}
                                onCloseRequest={() =>  setIsOpen(false)}
                        /></div>)
            }
        </div>
    )
};

export default LightBoxComp;