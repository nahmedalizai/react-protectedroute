import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import ReactQuill from 'react-quill'
import config from 'react-global-configuration'
import axios from 'axios'
import { useAlert } from 'react-alert'
import {Base64} from 'js-base64'

export default function UpdateAnswer(props) {
    const reactAlert = useAlert()
    const [answerid] = React.useState(props.answerid)
    const [open, setOpen] = React.useState(false);
    const [editor, setEditor] = React.useState({text: props.description});
    function handleClickOpen() {
        setOpen(true);
    }

    function handleClose() {
        setOpen(false);
    }

    function handleupdate() {
        if(editor.text === undefined || editor.text === null || editor.text === '')
            reactAlert.error('Please write something to submit')
        else {
                var bodyFormData = new FormData();  
                bodyFormData.set('method', config.get('apiMethods').updateAnswerById);
                axios({
                    method: 'post',
                    url: config.get('apiurl') + config.get('apiMethods').updateAnswerById,
                    data: bodyFormData,
                    params: {
                        description: Base64.encode(editor.text),
                        id: answerid
                    },
                    config: { headers: {'Content-Type': 'application/json'}}
                })
                .then((response) => {
                    if(response != null && response.status === 201){
                        reactAlert.success('Answer updated')
                        setOpen(false)
                    }
                    else
                        alert(response.data)
                })
            .catch((error) => { alert(error) })
        }
    }
    function handleEditorChange(value) {
        setEditor({text: value});
    }

    return (<div style={{float:'inherit'}}> 
                <button className='btn btn-link btn-sm' onClick={handleClickOpen}>
                    edit answer
                </button>
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Edit Answer</DialogTitle>
                    <DialogContent>
                        <ReactQuill value={editor.text} onChange={(value) => handleEditorChange(value)} />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleupdate} color="primary">
                            update
                        </Button>
                        <Button onClick={handleClose} color="primary">
                            close
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
    );
}