import React, {useEffect} from 'react'
import Table from 'react-bootstrap/Table'
import ReactQuill from 'react-quill'
import {Markup} from 'interweave'
import {Base64} from 'js-base64'
import LightBoxComp from './lightbox'
import {AuthConsumer} from '../auth'
import config from 'react-global-configuration'
import axios from 'axios'
import { useAlert } from 'react-alert'
import Fab from '@material-ui/core/Fab'
import {FaAngleLeft,FaCheck, FaRedo} from 'react-icons/fa'
import UpdateAnswer from './updateanswer'
import Tooltip from '@material-ui/core/Tooltip'
import {Popconfirm, message} from 'antd'
import 'antd/dist/antd.css'

message.config({
    top: 60,
    duration:3
  });


const ItemDetails = (props) => {
    const reactAlert = useAlert()
    const [disableReply, setDisableReply] = React.useState(false)
    const [state] = React.useState(props.location.state.question)
    const [question, setQuestion] = React.useState([])
    const [questionCategory, setQuestionCategory] = React.useState({category:'',subcategory:''})
    const [addedon, setAddedon] = React.useState('');
    const [user] = React.useState(props.location.state.user);
    const [data, setData] = React.useState([]);
    const [editor, setEditor] = React.useState({text: null});
    const thtdStyle = {
        'border': '1pt solid',
        'borderColor' : 'lightblue',
        'padding': '5px',
        'textAlign': 'justify',
        'borderRadius': '5px',
    }

    const rthStyle = {
        'padding': '5px',
    }

    function getCategoryById() {
        var bodyFormData = new FormData();
        bodyFormData.set('method', config.get('apiMethods').getSubcategoryNameById);
        axios({
            method: 'get',
            url: config.get('apiurl') + config.get('apiMethods').getSubcategoryNameById,
            data: bodyFormData,
            params: {
                id: state.vrp_subcategoryid
            },
            config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 200)
            {
                if(response.data !== []) {
                    var cat = response.data[0].category;
                    var subcat = response.data[0].subcategory;
                    setQuestionCategory({category:cat, subcategory:subcat})
                }
            }
        })
        .catch((error) => { alert(error) })
    }

    function closeButton(role) {
           return <Popconfirm
                    title="Are you sure you want to close this question?"
                    onConfirm={e =>handleClose(e,role)}
                    onCancel={cancelClose}
                    okText="Yes"
                    cancelText="No"
                 >
                    <button className='btn btn-danger btn-sm'
                                id="closeButton"
                                style={{float: 'right'}}>
                        Close Question</button>
                </Popconfirm>
    }

    function closeQuestion() {
        var bodyFormData = new FormData();
        bodyFormData.set('method', config.get('apiMethods').changeQuestionStatusById);
        axios({
            method: 'post',
            url: config.get('apiurl') + config.get('apiMethods').changeQuestionStatusById,
            data: bodyFormData,
            params: {
                status: 'Closed',
                id: state.id
            },
            config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 201)
            {
                reactAlert.success(response.data)
                document.getElementById("closeButton").disabled = true
            }
            else {
                alert(response.data)
            }
        })
        .catch((error) => { alert(error) })
    }

    function handleClose(e,role) {
        if(role.CRUD)
            closeQuestion()
        else if(role.Q_CRU && state.vrp_hasverifiedanswer)
            closeQuestion()
        else
            reactAlert.error('Answer not verified yet')
    }

    function getAnswersById() {
        var bodyFormData = new FormData();  
        bodyFormData.set('method', config.get('apiMethods').getAnswersById);
        axios({
            method: 'get',
            url: config.get('apiurl') + config.get('apiMethods').getAnswersById,
            data: bodyFormData,
            params: {
                questionid: state.id
            },
            config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 200) {
                setData(response.data)
            }
            else {
                alert(response.data)
            }
        })
    .catch((error) => { alert(error) })
    }

    function getQuestionById() {
        var bodyFormData = new FormData();  
        bodyFormData.set('method', config.get('apiMethods').getQuestionById);
        axios({
            method: 'get',
            url: config.get('apiurl') + config.get('apiMethods').getQuestionById,
            data: bodyFormData,
            params: {
                id: state.id
            },
            config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 200)
                setQuestion(response.data)
        }).then(getAnswersById())
    .catch((error) => { alert(error) })
    }

    function getFormattedDate(rawDate) {
        if(rawDate !== undefined && rawDate !== null)
        {
            var addedon = new Date(rawDate)
            var date = addedon.getDate();
            var month = addedon.getMonth(); 
            var year = addedon.getFullYear();
            return ((month+1) + "/" + date + "/" + year);
        }
        else 
            return ''

    }

    useEffect(() => {
        getQuestionById() // eslint-disable-next-line
        getCategoryById() // eslint-disable-next-line
        setAddedon(getFormattedDate(state.vrp_addedon)) // eslint-disable-next-line
    }, [])

    function handleVerify(event, answerid) {
        var bodyFormData = new FormData();
        bodyFormData.set('method', config.get('apiMethods').verifyAnswerById);
        axios({
            method: 'post',
            url: config.get('apiurl') + config.get('apiMethods').verifyAnswerById,
            data: bodyFormData,
            params: {
                answerid: answerid,
                questionid: state.id
            },
            config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 201)
            {
                reactAlert.success(response.data)
                getQuestionById()
            }
            else {
                reactAlert.error(response.data)
            }
        })
        .catch((error) => { alert(error) })
    }

    function handleReply(event) {
        event.preventDefault();
        if(editor.text === undefined || editor.text === null || editor.text === '')
            reactAlert.error('Please write something to submit')
        else {
                var bodyFormData = new FormData();  
                bodyFormData.set('method', config.get('apiMethods').addNewAnswer);
                axios({
                    method: 'post',
                    url: config.get('apiurl') + config.get('apiMethods').addNewAnswer,
                    data: bodyFormData,
                    params: {
                        description: Base64.encode(editor.text),
                        user: user.displayname,
                        questionid: state.id
                    },
                    config: { headers: {'Content-Type': 'application/json'}}
                })
                .then((response) => {
                    if(response != null && response.status === 201)
                    {
                        getQuestionById()
                        reactAlert.success('Answer submitted')
                        setEditor({text:null})
                        if(state.vrp_questiontype === 1) // single answer
                            setDisableReply(true)
                    }
                    else {
                        alert(response.data)
                    }
                })
            .catch((error) => { alert(error) })
            setEditor({text:null})
        }
    }

    function handleEditorChange(value) {
        setEditor({text: value});
    }

    function decodeHtml(html) {
        if(html === undefined || html === null || html === '')
            return '';    
        return Base64.decode(html);
    }
     
    function cancelVerify(e) {
        message.error('Verifying answer cancelled');
    }

    function cancelClose(e) {
        message.error('Closing question cancelled');
    }

    function markVerified(isVerified) {
        if(isVerified)
            return <span style={{margin: '2px'}} className="badge badge-success">{' Verified answer '}<FaCheck/></span>
        return ''
    }

    const _bindListItems = data.map(function(el, index) {
        return <tr key={index} id={el.id}>
                    <td style={thtdStyle}>
                        <Markup content={decodeHtml(el.vrp_description)}/>
                        <div>
                            <span style={{margin: '2px'}} className="badge badge-secondary">{' ' + el.vrp_answeredby + ' '}</span>
                            <span style={{margin: '2px'}} className="badge badge-secondary">{' ' + getFormattedDate(el.vrp_answeredon) + ' '}</span>
                            <React.Fragment>{markVerified(el.vrp_isverified)}</React.Fragment>
                            <AuthConsumer>{
                                ({role}) => {
                                    //debugger
                                    if(question !== undefined && question.length > 0 && !question[0].vrp_hasverifiedanswer && role.A_CRU)  {
                                        return <div style={{float:'left'}}>
                                                    <Popconfirm
                                                        title="Are you sure you want to verify this answer?"
                                                        onConfirm={e =>handleVerify(e,el.id)}
                                                        onCancel={cancelVerify}
                                                        value={el.id}
                                                        okText="Yes"
                                                        cancelText="No"
                                                    >
                                                        <button id="verifyButton" 
                                                            className='btn btn-link btn-sm'
                                                            value={el.id}>verify</button>
                                                    </Popconfirm>
                                                    <UpdateAnswer answerid={el.id} description={decodeHtml(el.vrp_description)}/>
                                            </div>
                                    }
                                    else if (role.CRUD)
                                        return <div><UpdateAnswer answerid={el.id} description={decodeHtml(el.vrp_description)}/></div>
                                    return null
                                }}
                            </AuthConsumer>
                        </div>
                    </td>
                    {/* <td style={sthtdStyle}>{el.answeredby}</td>
                    <td style={sthtdStyle}>{el.answeredon}</td> */}
                </tr>
    });
    
    function listItemsComponent() {
        return <div className='answerTableContainer'>
            <Table striped bordered>
                <thead>
                    <tr>
                        <th style={rthStyle}>Replies</th>
                    </tr>
                </thead>
                <tbody>
                    {_bindListItems}
                </tbody>
            </Table></div>
    }

    function _replyComponent() {
        return <div className='replyFormLayout'>
                    <form onSubmit={(event) => handleReply(event)}>
                        <ReactQuill value={editor.text} onChange={(value) => handleEditorChange(value)} />
                        <input disabled={disableReply} className='replyEditorButtonSubmit btn btn-primary btn-sm' type="submit" value="Submit" />
                    </form>
                    <button className='replyEditorButtonCancel btn btn-danger btn-sm' onClick={() => {
                            setEditor({text: null});
                    }}>Cancel</button>
                </div>
    }

    return <div>
            <div>
                <div className='overviewContainer'>
                    <h5 style={{marginLeft:'10px'}}>Details</h5>
                </div>
                <div className='backButtonContainer'>
                    <Tooltip title="Refresh answers">
                        <Fab style={{margin:'5px'}} size="small" color="default" onClick={getQuestionById}>
                            <FaRedo/>
                        </Fab>
                    </Tooltip>
                    <Tooltip title='back'>
                        <Fab style={{margin:'5px'}} size="small" color="primary" 
                            onClick={() => {
                                props.history.push("/app"); 
                            }}>
                            <FaAngleLeft/>
                        </Fab>
                    </Tooltip>
                </div>
                <div className='question'>
                    <h6><b>#{state.id + ' Question :  '}</b>{state.vrp_title}</h6>
                    <Markup content={decodeHtml(state.vrp_description)}/>
                    <LightBoxComp attachmentid={state.vrp_attachmentid}/>
                    <AuthConsumer>{({role}) => {
                        if((role.CRUD || role.Q_CRU) && state.vrp_status === 'Open')
                            return closeButton(role)
                        return null
                    }}</AuthConsumer>
                    <span style={{margin: '2px'}} className="badge badge-secondary">Posted by : {state.vrp_addedby}</span>
                    <span style={{margin: '2px'}} className="badge badge-secondary">Posted on : {addedon}</span>
                    <span style={{margin: '2px'}} className="badge badge-secondary">Category : {questionCategory.category}</span>
                    <span style={{margin: '2px'}} className="badge badge-secondary">Subcategory: {questionCategory.subcategory}</span>
                </div>
            </div>
            {listItemsComponent()}            
            {((state.vrp_addedby) !== (user.displayname) && !state.vrp_hasverifiedanswer && state.vrp_questiontype === 1 && state.answerid === null && state.vrp_status === 'Open') && _replyComponent()}
            {(!state.vrp_hasverifiedanswer && state.vrp_questiontype === 2 && state.vrp_status === 'Open') && _replyComponent()}

    </div>
};

export default ItemDetails;