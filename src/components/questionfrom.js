import React, {useEffect} from 'react'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import config from 'react-global-configuration'
import axios from 'axios'
import imageCompression from 'browser-image-compression'
import {AuthConsumer} from '../auth'
import {Base64} from 'js-base64'
import { useAlert } from 'react-alert'
import Linearprogress from '@material-ui/core/LinearProgress'

function QuestionForm(props) {
    const reactAlert = useAlert();
    const [isLoading, setIsLoading] = React.useState(false);
    const [title, setTitle] = React.useState('');
    const [editor, setEditor] = React.useState({text: ''});
    const [upload, setUpload] = React.useState({file: null});
    const [filePath, setFilePath] = React.useState(null);
    const [subCategories, setSubCategories] = React.useState([]);
    const [categories,setCategories] = React.useState([]);
    const [selectedQuestionType, setSelectedQuestionType] = React.useState();
    const [selectedCategory, setSelectedCategory] = React.useState();
    const [selectedSubCategory, setSelectedSubCategory] = React.useState();
    const [user, setUser] = React.useState(null);
    
    useEffect(() => {
        getCategories();
    }, [])

    function handleEditorChange(value) {
        setEditor({text: value});
    }

    function handleTitleChange(event) {
        setTitle(event.target.value);
    }

    function uploadQuestion(attachmentid) {
        setIsLoading(true)
        var bodyFormData = new FormData();  
        bodyFormData.set('method', config.get('apiMethods').addNewQuestion);
        axios({
        method: 'post',
        url: config.get('apiurl') + config.get('apiMethods').addNewQuestion,
        data: bodyFormData,
        params: {
            title: title,
            description: Base64.encode(editor.text),
            questiontypeid: selectedQuestionType,
            categoryid: selectedCategory,
            subcategoryid: selectedSubCategory,
            user: user.displayname,
            attachmentid: attachmentid,
        },
        config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 201){
                reactAlert.success(response.data)
                resetForm()
                setIsLoading(false)
            }
        }) 
        .catch((error) => { 
            alert(error) 
            setIsLoading(false)
        })
    }

    function uploadAttachmentAndQuestion() {
        setIsLoading(true)
        var bodyFormData = new FormData();  
        bodyFormData.set('file',upload.file);
        axios({
            method: 'post',
            url: config.get('apiurl') + config.get('apiMethods').addAttachment,
            data: bodyFormData,
            params: {
            },
            config: { headers: {'Content-Type': 'multipart/form-data'}}
        })
        .then((response) => {
            if(response !=null && response.data.isSuccess){
                uploadQuestion(response.data.attachmentid)
            }

        }) 
        .catch((error) => { 
            alert(error)
            setIsLoading(false) 
        })
    }

    function handleSubmit(event) { 
        if(upload.file != null)
            uploadAttachmentAndQuestion()
        else
            uploadQuestion()
    event.preventDefault();
    }

    function handleUploadChange(event) {
        if(event.target.files.length > 0){
            const imageFile = event.target.files[0];
            if(imageFile.type.substr(0, imageFile.type.indexOf('/')) === 'image')
                setFileforUpload(imageFile)
            else
                reactAlert.error('Given file is not an image')  
        }
    }

    function onPasteHandle(event) {
        var blob = null
        if(event.clipboardData !== false)
        {
            var items = event.clipboardData.items;
            if(items !== undefined)
            for (var i = 0; i < items.length; i++) {
                if (items[i].type.indexOf("image") === -1) continue;
                    blob = items[i].getAsFile();
            }
        }
        if(blob !== null) {
            setFileforUpload(blob);
        }   
        event.preventDefault();
    }

    function setFileforUpload(imageFile) {
        setFilePath(URL.createObjectURL(imageFile))
        var options = {
            maxSizeMB: 1,
            maxWidthOrHeight: 1920,
            useWebWorker: true
        }
        imageCompression(imageFile, options)
        .then(function (compressedFile) {
          setUpload({file: compressedFile})
        })
        .catch(function (error) {
          alert(error.message);
        });
    }

    function handleOnCategoryChange(event) {
        setSelectedCategory(event.target.value)
        setSubCategories(null)
        var bodyFormData = new FormData();  
            bodyFormData.set('method', config.get('apiMethods').getSubcategoriesById);
            axios({
            method: 'get',
            url: config.get('apiurl') + config.get('apiMethods').getSubcategoriesById,
            data: bodyFormData,
            params: {id: event.target.value},
            config: { headers: {'Content-Type': 'application/json'}}
            })
            .then((response) => {
                if(response != null && response.status === 200)
                {
                    var _options = response.data.map(function(element, index) {
                        return <option key={index} value={element.id}>{element.vrp_name}</option>
                    });
                    setSubCategories(_options)
                }
            }) 
            .catch((error) => { alert(error) })
    }

    function handleOnSubCategoryChange(event) {
        setSelectedSubCategory(event.target.value)
    }

    function handleOnQuestionTypeChange(event) {
        setSelectedQuestionType(event.target.value)
    }

    function resetForm(){
        setTitle('');
        setEditor({text: ''});
        setUpload({file: null});
        setFilePath(null);
        // setSelectedCategory(null)
        // setSelectedSubCategory(null)
        // setSelectedQuestionType(null)
        document.getElementById('attachment').src = '';
    }

    function getCategories() {
        var bodyFormData = new FormData();  
            bodyFormData.set('method', config.get('apiMethods').getCategories);
            axios({
            method: 'get',
            url: config.get('apiurl') + config.get('apiMethods').getCategories,
            data: bodyFormData,
            params: {},
            config: { headers: {'Content-Type': 'application/json'}}
            })
            .then((response) => {
                if(response != null && response.status === 200)
                {
                    var _options = response.data.map(function(element, index) {
                        return <option key={index} value={element.id}>{element.vrp_name}</option>
                    });
                    setCategories(_options)
                }
            }) 
            .catch((error) => { alert(error) })
    }

    function questionFormComponent() {
        return <div className='questionForm'>
            <AuthConsumer>{({user})=>{setUser(user)}}</AuthConsumer>
                            <form className='form' onSubmit={(event) => handleSubmit(event)}>
                                {isLoading && <Linearprogress/>}
                                <div className="form-group">
                                    <label>Question type</label>
                                    <select required className="form-control" onChange={(event) => handleOnQuestionTypeChange(event)}>
                                       <option value='' selected disabled>-- select option --</option>
                                       <option id='1' value='1'>Single Answer</option>
                                       <option id='2' value='2'>Discussion</option>
                                    </select>
                                </div>
                                <div className="row">
                                    <div className='col'>
                                        <label>Select Category</label>
                                        <select required className="form-control" onChange={(event) => handleOnCategoryChange(event)}>
                                        <option value='' selected disabled>-- select option --</option>
                                        {categories}
                                        </select>
                                    </div>
                                    <div className='col'>
                                        <label>Select Sub Category</label>
                                        <select required className="form-control" onChange={(event) => handleOnSubCategoryChange(event)}>
                                            <option value='' selected disabled>-- select option --</option>
                                            {subCategories}
                                        </select>
                                    </div>
                                </div>
                                <label>Add Title</label>
                                <input className='textInputTitle' required
                                            type="text"
                                            placeholder="Add title here"
                                            value={title}
                                            onChange={(event) => handleTitleChange(event)}
                                />
                                <ReactQuill value={editor.text} onChange={(value) => handleEditorChange(value)} />
        
                                <div className='upload-wrap'>
                                    <div className='floatLeft'>
                                        <input type='text' placeholder='Paste image here or ' onPaste={(event) => onPasteHandle(event)} />
                                        <input type="file" accept="image/*" onChange={(event) => handleUploadChange(event)}/>
                                    </div>
                                    <div className='floatRight'>
                                        <input className='formButton btn btn-primary btn-sm' type="submit" value="Submit" />
                                    </div>
                                </div>
                                <div>
                                <img id='attachment' alt={''} src={filePath}/>
                                </div>
                            </form>        
            </div>
    }

    return questionFormComponent()
}

export default QuestionForm;