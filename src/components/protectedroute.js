import React from 'react';
import {AuthConsumer} from '../auth';
import {Route, Redirect} from 'react-router-dom';

const ProtectedRoute = ({component: Component, ...rest}) => {
    return (
        <AuthConsumer>
            {
                ({isAuthenticated}) => (
                    <Route
                        {...rest}
                        render={props => 
                            isAuthenticated ? <Component {...props} /> : <Redirect to= {{
                                pathname: "/",
                                state: {
                                    from: props.location
                                }}}/>
                        }
                    />
                )
            }
        </AuthConsumer>
    )
}

export default ProtectedRoute;