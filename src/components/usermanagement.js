import React, {useEffect} from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'
import TextField from '@material-ui/core/TextField'
import Fab from '@material-ui/core/Fab'
import {FaSistrix} from 'react-icons/fa'
import Tooltip from '@material-ui/core/Tooltip'
import config from 'react-global-configuration'
import axios from 'axios'

export default function UserManagement(props) {
    const [disabled,setDisabled] = React.useState(true)
    const [successMessage,setSuccessMessage] = React.useState('')
    const [errorMessage,setErrorMessage] = React.useState('')
    const [isSuccess,setIsSuccess] = React.useState(false)
    const [isError,setIsError] = React.useState(false)
    const [open, setOpen] = React.useState(false);
    const [key, setKey] = React.useState(1);
    const [search, setSearch] = React.useState({text:''});
    const [roles, setRoles] = React.useState();
    const [teams, setTeams] = React.useState();
    const [user, setUser] = React.useState({
        id: null,
        name: '',
        username: '',
        email: '',
        teamid: null,
        roleid: null,
        password: '',
        confirmpassword: '',
        active:true
    })

    useEffect(() => {
        _getRoles()
        _getTeams()
    }, [])
    
    function handleClickOpen() {
        setOpen(true)
        setUserEmpty()
        setSearch({text:''})
        setDisabled(true)
        setMessageEmpty()
    }

    function handleClose() {
        setOpen(false);
    }

    const handleSearchChange = event => {
        setSearch({text: event.target.value})
    }

    function handleSearchClick() {
        setIsSuccess(false)
        setSuccessMessage('')
        setIsError(false)
        setErrorMessage('')
        if(search.text !== '')
        {
            var bodyFormData = new FormData();  
            bodyFormData.set('method', config.get('apiMethods').getUserDetails);
            axios({
                method: 'get',
                url: config.get('apiurl') + config.get('apiMethods').getUserDetails,
                data: bodyFormData,
                params: {
                    searchkey: search.text
                },
                config: { headers: {'Content-Type': 'application/json'}}
            })
            .then((response) => {
                if(response != null && response.status === 200) {
                    var data = response.data
                    setUser({
                        id: data.id,
                        name: data.vrp_name,
                        username: data.vrp_username,
                        email: data.vrp_email,
                        teamid: data.vrp_teamid,
                        roleid: data.vrp_roleid,
                        active: data.vrp_isactive
                    })
                    setDisabled(false)
                }
                else if(response != null && response.status === 202) {
                    setIsError(true)
                    setErrorMessage(response.data)
                    setUserEmpty()
                    setDisabled(true)
                }
            })
            .catch((error) => { alert(error) })
        }
    }

    function _getRoles() {
        var bodyFormData = new FormData();  
        bodyFormData.set('method', config.get('apiMethods').getRoles);
        axios({
        method: 'get',
        url: config.get('apiurl') + config.get('apiMethods').getRoles,
        data: bodyFormData,
        params: {},
        config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 200)
            {
                var _options = response.data.map(function(element, index) {
                    return <option key={index} value={element.id}>{element.vrp_rolename}</option>
                });
                setRoles(_options)
            }
        }) 
        .catch((error) => { alert(error) })
    }

    function _getTeams() {
        var bodyFormData = new FormData();  
        bodyFormData.set('method', config.get('apiMethods').getTeams);
        axios({
        method: 'get',
        url: config.get('apiurl') + config.get('apiMethods').getTeams,
        data: bodyFormData,
        params: {},
        config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 200)
            {
                var _options = response.data.map(function(element, index) {
                    return <option key={index} value={element.id}>{element.vrp_teamname}</option>
                });
                setTeams(_options)
            }
        }) 
        .catch((error) => { alert(error) })
    }

    function handleChange(event) {
        if(event.target.name === 'name')
            setUser({...user,name:event.target.value})
        else if(event.target.name === 'username')
            setUser({...user,username:event.target.value})
        else if(event.target.name === 'email')
            setUser({...user,email:event.target.value})
        else if(event.target.name === 'password')
            setUser({...user,password:event.target.value})
        else if(event.target.name === 'confirmpassword')
            setUser({...user,confirmpassword:event.target.value})
        else if(event.target.name === 'team')
            setUser({...user,teamid:event.target.value})
        else if(event.target.name === 'role')
            setUser({...user,roleid:event.target.value})
        else if(event.target.name === 'active')
            setUser({...user,active:event.target.value})
    }

    function addNewUser(event) {
        setMessageEmpty()
        if(user.password === user.confirmpassword)
        {
            var bodyFormData = new FormData();  
            bodyFormData.set('method', config.get('apiMethods').addUser);
            axios({
            method: 'post',
            url: config.get('apiurl') + config.get('apiMethods').addUser,
            data: bodyFormData,
            params: {
                vrp_name: user.name,
                vrp_username: user.username,
                vrp_email: user.email,
                vrp_teamid: user.teamid,
                vrp_roleid: user.roleid,
                vrp_password: user.password
            },
            config: { headers: {'Content-Type': 'application/json'}}
            })
            .then((response) => {
                if(response !== null && response.status === 201) {
                    setIsSuccess(true)
                    setSuccessMessage(response.data)
                    setUserEmpty()
                }
                else if(response !== null && response.status === 202) {
                    setIsError(true)
                    setErrorMessage(response.data)
                }                  
            }) 
            .catch((error) => { alert(error) })
        }
        else{
            setIsError(true)
            setErrorMessage('Password and confirm password are not the same')
        }
        event.preventDefault()
    }

    function setUserEmpty() {
        setUser({
            id: null,
            name: '',
            username: '',
            email: '',
            teamid: null,
            roleid: null,
            password: '',
            confirmpassword: '',
            active:true
        })
    }

    function setMessageEmpty() {
        setIsSuccess(false)
        setSuccessMessage('')
        setIsError(false)
        setErrorMessage('')
    }

    function updateUser(event) {
        if(user.password !== undefined && user.password!== null && user.password !== '')
            updateUserInfoAndPassword()
        else
            updateUserInformation()
        event.preventDefault()
    }

    function updateUserInfoAndPassword() {
        var bodyFormData = new FormData();  
        bodyFormData.set('method', config.get('apiMethods').changeUserPassword);
        axios({
        method: 'post',
        url: config.get('apiurl') + config.get('apiMethods').changeUserPassword,
        data: bodyFormData,
        params: {
            username: user.username,
            password: user.password
        },
        config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 201) {
                updateUserInformation()
            }
            else if(response !== null && response.status === 202) {
                setIsError(true)
                setErrorMessage(response.data)
            }
        }) 
        .catch((error) => { alert(error) })
    }

    function updateUserInformation() {
        var bodyFormData = new FormData();  
        bodyFormData.set('method', config.get('apiMethods').updateUserDetails);
        axios({
        method: 'post',
        url: config.get('apiurl') + config.get('apiMethods').updateUserDetails,
        data: bodyFormData,
        params: {
            id: user.id,
            name: user.name,
            username: user.username,
            email: user.email,
            teamid: user.teamid,
            roleid: user.roleid,
            active:user.active
        },
        config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 201) {
                setIsSuccess(true)
                setSuccessMessage(response.data)
                setUserEmpty()
                setSearch({text:''})
                setDisabled(true)
            }
            else if(response !== null && response.status === 202) {
                setIsError(true)
                setErrorMessage(response.data)
            }
        }) 
        .catch((error) => { alert(error) })
    }

    function onTabChange() {
        setUserEmpty()
        setSearch({text:''})
        setDisabled(true)
        setIsError(false)
        setIsSuccess(false)
        setErrorMessage('')
        setSuccessMessage('')
    }

    return (<div style={{float: 'left', margin: '2px'}}> 
                <Button size="small" variant="outlined" color="primary" onClick={handleClickOpen}>
                    Manage User
                </Button>
                <Dialog fullWidth={true} open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Manage User</DialogTitle>
                    <DialogContent>
                    <Tabs id="controlled-tab" activeKey={key} onClick={onTabChange} onSelect={k => setKey(k)}>
                        <Tab eventKey={1} title="Add User">
                            <form className='userFrom' onSubmit={(event) => addNewUser(event)}>
                                <div className='row'>
                                    <div className='col'>
                                        <label>Name</label>
                                        <input name='name' value={user.name} required className='form-control' placeholder='name' type='text' onChange={event=>handleChange(event)}/>
                                    </div>
                                    <div className='col'>
                                        <label>Username</label>
                                        <input name='username' value={user.username} required className='form-control' placeholder='username (no spaces)' type='text' onChange={event=>handleChange(event)}/>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col'>
                                        <label>Email</label>
                                        <input name='email' value={user.email} required className='form-control' placeholder='name@example.com' type='email' onChange={event=>handleChange(event)}/>
                                    </div>
                                </div>
                                <div className='row'>                                    
                                    <div className='col'>
                                        <label>Team</label>
                                        <select name='team' required className='form-control' onChange={event=>handleChange(event)}>
                                            <option value='' selected disabled>-- select option --</option>
                                            {teams}
                                        </select>
                                    </div>
                                    <div className='col'>
                                        <label>Role</label>
                                        <select name='role' required className='form-control' onChange={event=>handleChange(event)}>
                                            <option value='' selected disabled>-- select option --</option>
                                            {roles}
                                        </select>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col'>
                                        <label>Password</label>
                                        <input name='password' value={user.password} required className='form-control' placeholder='password' type='password' onChange={event=>handleChange(event)}/>
                                    </div>
                                    <div className='col'>
                                        <label>Confirm Password</label>
                                        <input name='confirmpassword' value={user.confirmpassword} required className='form-control' placeholder='confirm password' type='password' onChange={event=>handleChange(event)}/>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col'>
                                    <input type='submit' className='addUserSubmit btn btn-primary' value="Submit"/>
                                    </div>
                                </div>
                            </form>
                        </Tab>
                        <Tab eventKey={2} title="Edit User">
                            <div className='row'>
                                <div className='col'>
                                    <TextField
                                        autoFocus={true}
                                        margin="dense"
                                        id="name"
                                        value={search.text}
                                        onChange={event => handleSearchChange(event)}
                                        placeholder="Search by name/username"
                                        fullWidth
                                    />
                                </div>
                                <div className='col-2'>
                                    <Tooltip title="Search user">
                                        <Fab size="small" color="primary" onClick={handleSearchClick}>
                                            <FaSistrix/>
                                        </Fab>
                                    </Tooltip>
                                </div>
                            </div>
                            <form className='userFrom' onSubmit={(event) => updateUser(event)}>
                                <fieldset disabled={disabled}>
                                <div className='row'>
                                    <div className='col'>
                                        <label>Name</label>
                                        <input name='name' value={user.name} required className='form-control' placeholder='name' type='text' onChange={event=>handleChange(event)}/>
                                    </div>
                                    <div className='col'>
                                        <label>Username</label>
                                        <input name='username' value={user.username} required disabled={true} className='form-control' placeholder='username (no spaces)' type='text' onChange={event=>handleChange(event)}/>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col'>
                                        <label>Email</label>
                                        <input name='email' value={user.email} required className='form-control' placeholder='name@example.com' type='email' onChange={event=>handleChange(event)}/>
                                    </div>
                                </div>
                                <div className='row'>                                    
                                    <div className='col'>
                                        <label>Team</label>
                                        <select name='team' value={user.teamid} required className='form-control' onChange={event=>handleChange(event)}>
                                            <option value='' selected disabled>-- select option --</option>
                                            {teams}
                                        </select>
                                    </div>
                                    <div className='col'>
                                        <label>Role</label>
                                        <select name='role' value={user.roleid} required className='form-control' onChange={event=>handleChange(event)}>
                                            <option value='' selected disabled>-- select option --</option>
                                            {roles}
                                        </select>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col'>
                                        <label>Password</label>
                                        <input name='password' value={user.password} className='form-control' placeholder='password' type='password' onChange={event=>handleChange(event)}/>
                                    </div>
                                    <div className='col'>
                                        <label>Active/Deactive user</label>
                                        <select name='active' value={user.active} required className='form-control' onChange={event=>handleChange(event)}>
                                            <option value={true}>Activate</option>
                                            <option value={false}>Deactivate</option>
                                        </select>
                                    </div>
                                </div>
                                <div className='row'>
                                    <div className='col'>
                                    <input type='submit' className='addUserSubmit btn btn-primary' value="Update"/>
                                    </div>
                                </div>
                                </fieldset>
                            </form>
                        </Tab>

                    </Tabs>
                    </DialogContent>
                    <DialogActions>
                            <span style={{color:'green'}}>{isSuccess && successMessage}</span>
                            <span style={{color:'red'}}>{isError && errorMessage}</span>
                            <Button onClick={handleClose} color="primary">
                                Close
                            </Button>
                    </DialogActions>
                </Dialog>
            </div>
    );
}