import React from 'react';
import Spinner from 'react-loader-spinner'

export default function Loader() {
    return(
     <Spinner 
        type="Triangle"
        color="darkred"
        height="100"	
        width="100"
     />   
    );
}
