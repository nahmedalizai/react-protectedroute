import React, {useEffect} from 'react'
import {AuthConsumer} from '../auth'
import QuestionList from './questionlist'
import QuestionForm from './questionfrom'
import config from 'react-global-configuration'
import axios from 'axios'
import UserManagement from './usermanagement'
import AddCategory from './addcategory'
import Fab from '@material-ui/core/Fab'
import {FaRedo, FaPlus, FaList} from 'react-icons/fa'
import Tooltip from '@material-ui/core/Tooltip'
import Loader from './loader'

const AppLayout = props => {
    const [user, setUser] = React.useState()
    const [isLoader,setIsLoader] = React.useState(true)
    const [questionView, setQuestionView] = React.useState(false)
    const [data, setData] = React.useState()
    const [dataLoaded, setDataLoaded] = React.useState(false)
    const [selected, setSelected] = React.useState(2)
    const [categories,setCategories] = React.useState([])
    const [selectedCat, setSelectedCat] = React.useState(0)

    useEffect(() => {
        refreshList() // eslint-disable-next-line
        getCategories() // eslint-disable-next-line
    }, [])
    
    function onAddNewQuestionButtonClick() {
        setDataLoaded(false)
        if(!questionView)
            setQuestionView(true)
    }

    function onCancelButtonClick() {
        if(questionView)
         {   
            setQuestionView(false)
            fetchData("2",null,"0")
         }
    }

    function manageButtons(role) {
        if(role.CRUD)
            return <React.Fragment><UserManagement/><AddCategory/></React.Fragment>
        else if(role.Q_CRU)
            return <React.Fragment><AddCategory/></React.Fragment>
        else
            return null 
    }

    function handleFilterChange(event) {
        setSelected(event.target.value)
        if(event.target.value === "1"){
            document.getElementById('filterSelectCat').disabled = true
            fetchData(event.target.value,null,selectedCat)
        }
        else {
            document.getElementById('filterSelectCat').disabled = false
            if(event.target.value === "4")
                fetchData(event.target.value,user.displayname,selectedCat)    
            else
                fetchData(event.target.value,null,selectedCat)
        }
    }

    function handleCategoryChange(event) {
        setSelectedCat(event.target.value)
        if(selected === "4")
            fetchData(selected,user.displayname,event.target.value)    
        else
            fetchData(selected,null,event.target.value)
    }

    function fetchData(type,value,categoryid) {
        setIsLoader(true)
        setDataLoaded(false)
        var bodyFormData = new FormData();  
        bodyFormData.set('method', config.get('apiMethods').getQuestions);
        axios({
        method: 'get',
        url: config.get('apiurl') + config.get('apiMethods').getQuestions,
        data: bodyFormData,
        params: {
            filterType: type,
            filterValue: value,
            categoryid: categoryid
        },
        config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 200)
            {
                setData(response.data)
                setIsLoader(false)
                setDataLoaded(true)
            }
        }) 
        .catch((error) => { alert(error) })
    }

    function getCategories() {
        var bodyFormData = new FormData();  
            bodyFormData.set('method', config.get('apiMethods').getCategories);
            axios({
            method: 'get',
            url: config.get('apiurl') + config.get('apiMethods').getCategories,
            data: bodyFormData,
            params: {},
            config: { headers: {'Content-Type': 'application/json'}}
            })
            .then((response) => {
                if(response != null && response.status === 200)
                {
                    var _options = response.data.map(function(element, index) {
                        return <option key={index} value={element.id}>{element.vrp_name}</option>
                    });
                    setCategories(_options)
                }
            }) 
            .catch((error) => { alert(error) })
    }

    function refreshList() {
        if(!questionView)
        {
            setSelected(2) //status open
            setSelectedCat(0) // category all
            var element = document.getElementById('filterSelect');
            element.value = 2;
            element = document.getElementById('filterSelectCat');
            element.value = 0;
            fetchData("2",null,"0")
        }
    }

    function _filter() { return <div className='row'>
        <div style={{marginLeft:'10px'}}>
            <Tooltip title="Refresh list">
                    <Fab style={{marginTop:'20px'}} size="small" color="default" onClick={refreshList}>
                        <FaRedo/>
                    </Fab>
            </Tooltip>
        </div>
        <div className='col-3'>
            <label>Filter by</label>
            <select id="filterSelect" defaultValue={selected} className="form-control form-control-sm" onChange={handleFilterChange}>
                <option value={1}>All</option>
                <option value={2}>Status : Open</option>
                <option value={3}>Status : Closed</option>
                <option value={4}>Posted by me</option>
                <option value={5}>Not Answered yet</option>
                <option value={6}>Not Verified yet</option>
            </select>
        </div>
        <div className='col-3'>
            <label>Category</label>
            <select id="filterSelectCat" defaultValue={selectedCat} className="form-control form-control-sm" onChange={handleCategoryChange}>
               <option value={0}>All</option>
               {categories}
            </select>
        </div>
    </div>}

    return <div className='dashboardConatiner'>
                <div className='overviewContainer'><h5>Overview</h5></div>
                <div className='newQuestionContainer'>
                    <AuthConsumer>{
                        ({role,user}) => {
                            setUser(user)           
                            if(role.CRUD || role.Q_CRU || role.Q_CR) {
                                return <React.Fragment>
                                        {manageButtons(role)}
                                        <Tooltip title="Questions list">
                                            <Fab style={{margin:'5px'}} size="small" color="secondary" onClick={onCancelButtonClick}>
                                                <FaList/>
                                            </Fab>
                                        </Tooltip>
                                        <Tooltip title="Add new question">
                                            <Fab style={{margin:'5px'}} size="small" color="primary" onClick={onAddNewQuestionButtonClick}>
                                                <FaPlus/>
                                            </Fab>
                                        </Tooltip>
                                    </React.Fragment>
                            }
                            return null
                        }}
                    </AuthConsumer>
                </div>
                {!questionView && _filter()}
                <div style={{textAlign:'center'}}>{isLoader && <Loader/>}</div>
                {!questionView && dataLoaded && <QuestionList data={data}/>}
                {questionView && <QuestionForm/>}
    </div>
}

export default AppLayout;