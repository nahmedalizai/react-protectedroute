import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import config from 'react-global-configuration';
import axios from 'axios';

export default function AddCategory(props) {
    const [open, setOpen] = React.useState(false);
    const [error, setError] = React.useState(false);
    const [success,setSuccess] = React.useState(false);
    const [categories,setCategories] = React.useState([]);
    const [selectedCategory, setSelectedCategory] = React.useState(null);
    const [categoryText, setCategoryText] = React.useState({cat: '',subcat: ''})
    const [required,setRequired] = React.useState(true)

    function handleClickOpen() {
        setOpen(true);
        setError(false)
        setSuccess(false)
        getCategories()  
    }

    function handleClose() {
        setOpen(false);
        setCategories([])
    }

    function handleCategoryChange(event) {
        setCategoryText({...categoryText, cat:event.target.value})
        if(event.target.value !== '')
            setRequired(false)
        else
            setRequired(true)
    }
    
    function handleSubcategoryChange(event) {
        setCategoryText({...categoryText, subcat:event.target.value})
        setError(false)
    }

    function getCategories() {
        var bodyFormData = new FormData();  
            bodyFormData.set('method', config.get('apiMethods').getCategories);
            axios({
            method: 'get',
            url: config.get('apiurl') + config.get('apiMethods').getCategories,
            data: bodyFormData,
            params: {},
            config: { headers: {'Content-Type': 'application/json'}}
            })
            .then((response) => {
                if(response != null && response.status === 200)
                {
                    var _options = response.data.map(function(element, index) {
                        return <option key={index} value={element.id}>{element.vrp_name}</option>
                    });
                    setCategories(_options)
                }
            }) 
            .catch((error) => { alert(error) })
    }

    function addCategory(categoryName) {
        var bodyFormData = new FormData();  
            bodyFormData.set('method', config.get('apiMethods').addCategory);
            axios({
            method: 'post',
            url: config.get('apiurl') + config.get('apiMethods').addCategory,
            data: bodyFormData,
            params: {
                categoryname: categoryName
            },
            config: { headers: {'Content-Type': 'application/json'}}
            })
            .then((response) => {
                if(response !== null && response.data !== null) // There is check on server side, if category already exisited id will be returned otherwise new category will be created and id will be returned
                    addSubCategory(response.data[0].id)
            }) 
            .catch((error) => { alert(error) })
    }

    function addSubCategory(categoryid) {
        var bodyFormData = new FormData();  
            bodyFormData.set('method', config.get('apiMethods').addSubCategory);
            axios({
            method: 'post',
            url: config.get('apiurl') + config.get('apiMethods').addSubCategory,
            data: bodyFormData,
            params: {
                categoryid: categoryid,
                subcategoryname: categoryText.subcat
            },
            config: { headers: {'Content-Type': 'application/json'}}
            })
            .then((response) => {
                if(response != null && response.status === 201)
                {
                    setSuccess(true)
                    setError(false)
                    getCategories()
                }
                else if(response.status === 202){
                    setError(true)
                    setSuccess(false)
                }
            }) 
            .catch((error) => { alert(error) })
    }

    function handleOnCategoryChange(event) {
        setSelectedCategory(event.target.value)
    }

    function addNewCategory(event) {
        if(categoryText.cat !== '')
        {
            addCategory(categoryText.cat)
        }
        else
            addSubCategory(selectedCategory)
        event.preventDefault();
    }

    return (<div style={{float: 'left', margin:'2px'}}> 
                <Button size="small" variant="outlined" color="primary" onClick={handleClickOpen}>
                   Add Category
                </Button>
                <Dialog fullWidth={true} open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add New Category</DialogTitle>
                    <form onSubmit={(event) => addNewCategory(event)}>
                    <DialogContent>
                            <div className="form-group">
                                <label>Add New or select from existing category</label>
                                <div className="row">
                                        <div className='col'>
                                            <input required={!required} onChange={(event) => handleCategoryChange(event)} className="form-control" type='text' placeholder='add category name here'/>
                                        </div>
                                        <div className='col'>
                                            <select required={required} className="form-control" type='select' onChange={(event) => handleOnCategoryChange(event)}>
                                                <option value='' selected disabled>-- select option --</option>
                                                {categories}
                                            </select>
                                        </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Add subcategory</label>
                                <input required={true} onChange={(event) => handleSubcategoryChange(event)} className="form-control" type='text' placeholder='add subcategory name here'/>
                            </div>
                            {success && <label style={{color:'green'}}>Subcategory added</label>}
                            {error && <label style={{color:'red'}}>Subcategory already exists for this category</label>}
                    </DialogContent>
                    <DialogActions>
                    <Button type="submit" color="primary">
                            Add
                        </Button>
                        <Button onClick={handleClose} color="primary">
                            Close
                        </Button>
                    </DialogActions>
                    </form>
                </Dialog>
            </div>
    );
}