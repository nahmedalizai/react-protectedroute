import React from 'react'
import "react-table/react-table.css"
import {AuthConsumer} from '../auth'
import history from '../history'
import {Base64} from 'js-base64'
import config from 'react-global-configuration'
import axios from 'axios'
import { useAlert } from 'react-alert'
import { MDBDataTable } from 'mdbreact'
import {FaCheck, FaInfo, FaExclamation} from 'react-icons/fa'
import {Popconfirm} from 'antd'
import 'antd/dist/antd.css'

export default function QuestionList(props) {
    const reactAlert = useAlert()
    const [state] = React.useState({
        data: props.data
    })
    const rows = [{}]

    const data = {
        columns: [
          {
            field: 'rowData'
          }
        ],
        rows: rows
      };

    function handleViewDetailsClick(event,obj,user) {
        var bodyFormData = new FormData();  
        bodyFormData.set('method', config.get('apiMethods').getQuestionById);
        axios({
            method: 'get',
            url: config.get('apiurl') + config.get('apiMethods').getQuestionById,
            data: bodyFormData,
            params: {
                id: state.data[obj.index].id
            },
            config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 200)
                history.push("/question",{question:response.data[0],user:user})
        })
        .catch((error) => { alert(error) })
    }

    function closeQuestion(id) {
        var bodyFormData = new FormData();
        bodyFormData.set('method', config.get('apiMethods').changeQuestionStatusById);
        axios({
            method: 'post',
            url: config.get('apiurl') + config.get('apiMethods').changeQuestionStatusById,
            data: bodyFormData,
            params: {
                status: 'Closed',
                id: id
            },
            config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 201) {
                reactAlert.success(response.data)
                document.getElementById('close'+id).disabled = true
            }
            else {
                alert(response.data)
            }
        })
        .catch((error) => { alert(error) })
    }

    function handleCloseQuestion(event,role,index,id) {
        if(role.CRUD)
            closeQuestion(id)
        else if(role.Q_CRU) {
            if(state.data[index].answerid === null)
                reactAlert.error('Question not answered yet')    
            else if(state.data[index].vrp_hasverifiedanswer) {
                closeQuestion(id)
            }
            else
                reactAlert.error('Answer not verified yet')
        }
    }

    function handleVerify(event,questionid,answerid) {
        var bodyFormData = new FormData();
        bodyFormData.set('method', config.get('apiMethods').verifyAnswerById);
        axios({
            method: 'post',
            url: config.get('apiurl') + config.get('apiMethods').verifyAnswerById,
            data: bodyFormData,
            params: {
                questionid: questionid,
                answerid: answerid
            },
            config: { headers: {'Content-Type': 'application/json'}}
        })
        .then((response) => {
            if(response != null && response.status === 201) {
                reactAlert.success(response.data)
                document.getElementById('verify'+questionid).disabled = true
            }
            else {
                reactAlert.error(response.data)
            }
        })
        .catch((error) => { alert(error) })
    }

    function _decodeReply(encoded) {
        if(encoded !== undefined && encoded !== null){
            return <div><b>Answer : </b> {decodeHtml(Base64.decode(encoded))}</div>  
        }
        return null
    }

    function decodeHtml(html) {
        var div = document.createElement("div");
        div.innerHTML = html;
        return (div.textContent || div.innerText || "")
    }

    const _noAnswer = (
        <span className="badge badge-danger">{'Not answered yet '}<FaExclamation/></span>
    )

    const _verifiedAnswer = (
        <span className="badge badge-success">{' Verified '}<FaCheck/></span>
    )

    function _modifiedOn(rawdate) {
         var modDate = new Date(rawdate)
         var date = modDate.getDate();
         var month = modDate.getMonth(); 
         var year = modDate.getFullYear();
        return <span style={{marginRight: '5px',marginLeft: '5px'}} className="badge badge-info">Last modified on : {(month+1) + "/" + date + "/" + year}</span>
    }

    const _notVerifiedYet = (
        <span className="badge badge-warning">{' Not verified yet '}<FaInfo/></span>
    )

    function _questionType(value) {
        if(value !== undefined || value !== null){
            if(value === 1)
                return <span className="badge badge-secondary">Single answer</span>
            else if(value === 2)
                return <span className="badge badge-secondary">Discussion</span>
            else
                return null
        }
        else
            return null    
    }

    function _status(status) { 
        if(status !== undefined && status !== null && status !== '') {
            if (status === 'Open')
                return <span className="badge badge-success">{'Status : ' + status}</span>
            else
                return <span className="badge badge-danger">{'Status : ' + status}</span>
        }
        return null
    } 

    function _questions() {
         state.data.map(function(el, index) {
            return rows.push({rowData: <div style={{ padding: "10px", backgroundColor: "whitesmoke"}}>
                <div>
                    <div><span><b>{'#' + el.id +' '} Question : </b></span>{el.vrp_title}</div>
                    {_decodeReply(el.latestreply)}
                    {_questionType(el.vrp_questiontype)}
                    {_modifiedOn(el.vrp_modifiedon)}
                    {(el.answerid === null && el.vrp_status === 'Open') && _noAnswer}
                    {(!el.vrp_hasverifiedanswer && el.answerid !== null && el.vrp_status === 'Open') && _notVerifiedYet}
                    {el.isverifiedanswer && _verifiedAnswer}
                    <div style={{ float: "right"}}>
                        <AuthConsumer>{
                                    ({user}) => {
                                        return <button className='btn btn-link'
                                                    id={'details'+el.id}
                                                    value={el.id}
                                                    onClick={
                                                    (event) => handleViewDetailsClick(event,{id: el.id, index:index},user)}>
                                                view details</button>
                                    }
                            }
                        </AuthConsumer>
                        <AuthConsumer>{
                                    ({role}) => {
                                        if((role.CRUD || role.Q_CRU) && el.vrp_status === 'Open') {
                                            return <Popconfirm
                                                    title="Are you sure you want to close this question?"
                                                    onConfirm={e =>handleCloseQuestion(e,role, index,el.id)}
                                                    okText="Yes"
                                                    cancelText="No"
                                                >
                                                    <button className='btn btn-link'
                                                        id={'close'+el.id}>
                                                    close</button>
                                                </Popconfirm>
                                    }
                                    return null
                                }
                            }
                        </AuthConsumer>
                        <AuthConsumer>{
                                    ({role}) => {
                                        if((role.CRUD || role.A_CRU) && (el.answerid !== null && !el.vrp_hasverifiedanswer)) {
                                            return <Popconfirm
                                                    title="Are you sure you want to verify this answer?"
                                                    onConfirm={e =>handleVerify(e,el.id,el.answerid)}
                                                    okText="Yes"
                                                    cancelText="No"
                                                >
                                                    <button className='btn btn-link'
                                                        id={'verify'+el.id}>
                                                    verify</button>
                                                </Popconfirm> 
                                    }
                                    return null
                                }
                            }
                        </AuthConsumer>
                        {_status(el.vrp_status)}
                    </div>
                </div>
            </div>})
        })
    }

    return <div>
        {_questions()}
        <MDBDataTable 
            id='mdtable'
           // striped
           // small
            entries='50'
            searching={false}
            data={data}
        />
    </div>
}