import React from 'react';
import {AuthConsumer} from './auth';
import Loader from './components/loader';
import { Alert } from 'reactstrap';
import {FaSignInAlt} from 'react-icons/fa';
import Button from '@material-ui/core/Button'
import BG from './image-bg.png';
import config from 'react-global-configuration'

const LandingPage = (props) => {
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    function handleUsernameChange(event) {
        setUsername(event.target.value);
    }

    function handlePasswordChange(event) {
        setPassword(event.target.value);
    }

    const errorMessage = (
        <Alert color="danger">Wrong username or password </Alert>
    )

    return (
    <div style={{backgroundImage:'url(' +BG+')', backgroundRepeat:'no-repeat',backgroundSize:'cover', height: '93.9vh', marginTop:'-20px'}}>
        <AuthConsumer>
            { ({login, isLoading, authFailed}) => (
                <div className='align'>
                    <div className='loginContainer'>
                        <form className='editForm' onSubmit={(event)=>login(event,username,password)}>
                            <div className="form-group">
                                <input className="form-control" type='text' autoFocus required placeholder='Username' value={username} onChange={
                                    (event) => handleUsernameChange(event)
                                }/>
                            </div>
                            <div className="form-group">
                                <input className="form-control" type='password' required placeholder='Password' value={password} onChange={
                                    (event) => handlePasswordChange(event)}/>
                            </div>
                            <div className="form-group">
                                <Button variant='contained' color='default' className="loginButton" type="submit"><span style={{marginRight:'3%'}}>Login </span><FaSignInAlt/></Button>
                            </div>
                            <span style={{fontSize:'small', fontFamily: 'inherit'}}><b>{config.get('version')}</b></span>
                        </form>          
                    </div>
                    <div className='loaderContainer'>{isLoading && <Loader/>}</div>
                    <div className='loaderContainer'>{authFailed && errorMessage}</div>
                </div>
              )
            }
        </AuthConsumer>
    </div>
)};

export default LandingPage;