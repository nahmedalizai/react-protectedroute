import config from 'react-global-configuration';

var apiMethods = {
  addUser: 'adduser',
  verifyUser: 'login',
  changeUserStatus: 'changeuserstatus',
  changeUserPassword: 'changeuserpassword',
  updateUserDetails: 'updateuserdetails',
  getUserDetails: 'getuserdetails',
  getCategories: 'getcategories',
  getSubcategoriesById: 'getsubcategoriesbyid',
  addCategory: 'addcategory',
  addSubCategory: 'addsubcategory',
  addNewQuestion: 'addnewquestion',
  addAttachment: 'addattachment',
  getAttachment: 'getattachment',
  getQuestions: 'getquestions',
  addNewAnswer: 'addnewanswer',
  getAnswersById: 'getanswersbyid',
  verifyAnswerById: 'verifyanswerbyid',
  changeQuestionStatusById: 'changequestionstatusbyid',
  getCategoryNameById: 'getcategorynamebyid',
  getSubcategoryNameById: 'getsubcategorynamebyid',
  updateAnswerById: 'updateanswerbyid',
  getRoles: 'getroles',
  getTeams: 'getteams',
  getQuestionById: 'getquestionbyid'
}

if(window.location.hostname === 'localhost')
{
  config.set({ 
      apiurl: 'http://localhost:3300/',
      apiMethods : apiMethods,
      version: '0.3'
  });
}
else
  config.set({ 
      apiurl: 'http://192.168.103.96:3300/',
      apiMethods : apiMethods,
      version: '0.3'
  });