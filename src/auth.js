import React from 'react'
import history from './history'
import config from 'react-global-configuration';
import axios from 'axios';

const AuthContext = React.createContext()

class AuthProvider extends React.Component {
  state = {
    authFailed: false,
    isAuthenticated: false, 
    user: { 
      displayName: '',
      role: '',
      team: ''
      },
    hasRole: {
      CRUD: false,
      Q_CRU: false,
      A_CRU: false,
      Q_CR: false,
      A_CR: false
    },
    isLoading: false
   }

  constructor(props) {
    super(props)
    this.login = this.login.bind(this)
    this.logout = this.logout.bind(this)
  }

  login(event,username,password) {
    this.setState({...this.state, isLoading:true, authFailed:false})
    event.preventDefault();
    setTimeout(() => {
      if((username !== null && username !== "") && (password !== null && password !== "")) {
        var bodyFormData = new FormData();  
            bodyFormData.set('method', config.get('apiMethods').verifyUser);
            axios({
            method: 'get',
            url: config.get('apiurl') + config.get('apiMethods').verifyUser + '/', // Safari fix,
            data: bodyFormData,
            params: {username: username, password:password},
            config: { headers: {'Content-Type': 'application/json'}}
            })
            .then((response) => {
                if(response != null && response.status === 200)
                {
                  if(response.data.isSuccess) {
                    this.setState({ 
                      isAuthenticated: true, 
                      user: {
                        displayname: response.data.userDetails.displayName,
                        emailaddress: response.data.userDetails.emailAddress,
                        username: response.data.userDetails.username, 
                        team: response.data.userDetails.teamCode,
                        role: response.data.userDetails.roleCode,
                      }
                    })
                    this.setRole(this.state.user.role,this.state.user.team)
                    history.push("/app");
                  }
                }
                else{
                  this.setState({...this.state, authFailed:true})
                }
            }).catch((error) => { alert(error) })
      }
      this.setState({...this.state, isLoading:false})
    }, 500);
  }

  setRole(role,team) {
    if(role === 'CRUD')
      this.setState({...this.state, hasRole:{ CRUD: true, Q_CRU: false, A_CRU: false, Q_CR: false, A_CR: false}}) //admin user role
    else if(role === 'CRU' && team === 'QTN')
      this.setState({...this.state, hasRole:{ CRUD: false, Q_CRU: true, A_CRU: false, Q_CR: false, A_CR: false}}) //question team lead user role
    else if(role === 'CRU' && team === 'ANS')
      this.setState({...this.state, hasRole:{ CRUD: false, Q_CRU: false, A_CRU: true, Q_CR: false, A_CR: false}}) //answer team lead user role
    else if(role === 'CR' && team === 'QTN')
      this.setState({...this.state, hasRole:{ CRUD: false, Q_CRU: false, A_CRU: false, Q_CR: true, A_CR: false}}) //question team user role
    else if(role === 'CR' && team === 'ANS')
      this.setState({...this.state, hasRole:{ CRUD: false, Q_CRU: false, A_CRU: false, Q_CR: false, A_CR: true}}) //answer team user role
  }

  logout() {
    this.setState({ isAuthenticated: false })
  }

  render() {
    return (
      <div>
      <AuthContext.Provider
        value={{
          user: this.state.user,
          role: this.state.hasRole,
          isAuthenticated: this.state.isAuthenticated,
          login: this.login,
          logout: this.logout,
          isLoading: this.state.isLoading,
          authFailed: this.state.authFailed
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
      </div>
    )
  }
}

const AuthConsumer = AuthContext.Consumer

export { AuthProvider, AuthConsumer }